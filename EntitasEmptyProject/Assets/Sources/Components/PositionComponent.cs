using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

namespace DM.EntitasGame
{
	[ Game, Event( true ) ]
	public sealed class PositionComponent : IComponent
	{
		#region Public Fields
		public Vector3 value;
		#endregion
	}
}
