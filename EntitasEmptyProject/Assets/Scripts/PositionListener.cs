using Entitas;
using UnityEngine;

namespace DM.EntitasGame
{
	public class PositionListener : MonoBehaviour, IEventListener, IPositionListener
	{
		#region Private Fields
		GameEntity _entity;
		#endregion

		#region Public Members
		public void RegisterListeners( IEntity entity )
		{
			_entity = ( GameEntity ) entity;
			_entity.AddPositionListener( this );
		}

		public void OnPosition( GameEntity entity, Vector3 value )
		{
			transform.position = value;
		}
		#endregion
	}
}
