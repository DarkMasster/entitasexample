using Entitas;

namespace DM.EntitasGame
{
	[ Game ]
	public sealed class ViewComponent : IComponent
	{
		#region Public Fields
		public IViewController instance;
		#endregion
	}
}
