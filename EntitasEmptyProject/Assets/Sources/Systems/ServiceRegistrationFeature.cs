namespace DM.EntitasGame
{
	public class ServiceRegistrationFeature : Feature
	{
		#region Constructors
		public ServiceRegistrationFeature( Contexts contexts, Services services )
		{
			Add( new RegisterViewServiceSystem( contexts, services.View ) );
		}
		#endregion
	}
}
