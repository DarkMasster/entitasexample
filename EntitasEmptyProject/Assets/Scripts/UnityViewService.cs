using System;
using Entitas;
using Entitas.Unity;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DM.EntitasGame
{
	public class UnityViewService : IViewService
	{
		#region Public Members
		public IViewController Instantinate( Contexts contexts, IContext parentContext, IEntity entity, string prefabPath )
		{
			var viewGo = Object.Instantiate( Resources.Load< GameObject >( prefabPath ) );
			if( viewGo == null )
			{
				throw new ArgumentException( "UnityViewService: Can't find prefab by path = " + prefabPath );
			}

			var viewController = viewGo.GetComponent< IViewController >();
			if( viewController == null )
			{
				throw new NullReferenceException( "UnityViewService: Can't get IViewController from instantiate GameObject = " + viewGo.name + ". Add IViewController implementation on parent prefab." );
			}

			viewGo.Link( entity, parentContext );
			viewController.InitializeView( contexts, entity );

			var eventListeners = viewGo.GetComponents< IEventListener >();
			foreach( var listener in eventListeners )
			{
				listener.RegisterListeners( entity );
			}

			return viewController;
		}
		#endregion
	}
}
