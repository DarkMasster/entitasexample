﻿using Entitas;
using UnityEngine;

namespace DM.EntitasGame
{
	public class UnityGameView : MonoBehaviour, IViewController
	{
		#region Protected Fields
		protected Contexts _contexts;
		protected GameEntity _entity;
		#endregion

		#region Properties
		public bool Active { get { return gameObject.activeSelf; } set { gameObject.SetActive( value ); } }

		public Vector3 Position { get { return transform.position; } set { transform.position = value; } }

		public Vector3 Scale { get { return transform.localScale; } set { transform.localScale = value; } }
		#endregion

		#region Public Members
		public void InitializeView( Contexts contexts, IEntity entity )
		{
			_contexts = contexts;
			_entity = ( GameEntity ) entity;
		}

		public void DestroyView()
		{
			Destroy( this );
		}
		#endregion
	}
}