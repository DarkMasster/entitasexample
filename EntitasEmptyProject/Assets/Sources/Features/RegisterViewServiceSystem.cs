using Entitas;

namespace DM.EntitasGame
{
	public class RegisterViewServiceSystem : IInitializeSystem
	{
		#region Private Fields
		private readonly ProxyContext _proxyContext;
		private readonly IViewService _viewService;
		#endregion

		#region Constructors
		public RegisterViewServiceSystem( Contexts contexts, IViewService viewService )
		{
			_proxyContext = contexts.proxy;
			_viewService = viewService;
		}
		#endregion

		#region Public Members
		public void Initialize()
		{
			_proxyContext.ReplaceViewServiceProxy( _viewService );
		}
		#endregion
	}
}
