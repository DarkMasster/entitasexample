using Entitas;

namespace DM.EntitasGame
{
	public interface IEventListener
	{
		#region Public Members
		void RegisterListeners( IEntity entity );
		#endregion
	}
}
