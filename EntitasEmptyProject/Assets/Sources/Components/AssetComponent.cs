using Entitas;

namespace DM.EntitasGame
{
	[ Game ]
	public class AssetComponent : IComponent
	{
		#region Public Fields
		public string prefabPath;
		#endregion
	}
}