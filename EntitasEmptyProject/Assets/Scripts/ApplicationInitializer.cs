﻿using Entitas;
using UnityEngine;

namespace DM.EntitasGame
{
	public class ApplicationInitializer : MonoBehaviour
	{
		#region Private Fields
		private Systems _systems;
		#endregion

		#region Private Members
		void Start()
		{
			Random.InitState( 42 );

			var _services = new Services( new UnityViewService() );

			var contexts = Contexts.sharedInstance;

			_systems = new Feature( "Systems" );
			_systems.Add( new ServiceRegistrationFeature( contexts, _services ) );
			_systems.Add( new GameViewFeature( contexts ) );
			_systems.Add( new GameEventSystems( contexts ) );
			_systems.Add( new GameFeature( contexts ) );

			_systems.Initialize();
		}

		void Update()
		{
			_systems.Execute();
			_systems.Cleanup();
		}

		void OnDestroy()
		{
			_systems.TearDown();
		}
		#endregion
	}
}
