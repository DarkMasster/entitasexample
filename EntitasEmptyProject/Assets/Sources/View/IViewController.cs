﻿using Entitas;
using UnityEngine;

namespace DM.EntitasGame
{
	public interface IViewController
	{
		#region Properties
		Vector3 Position { get; set; }
		Vector3 Scale { get; set; }
		bool Active { get; set; }
		#endregion

		#region Public Members
		void InitializeView( Contexts contexts, IEntity Entity );
		void DestroyView();
		#endregion
	}
}