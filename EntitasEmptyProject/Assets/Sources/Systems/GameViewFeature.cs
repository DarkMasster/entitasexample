namespace DM.EntitasGame
{
	public class GameViewFeature : Feature
	{
		#region Constructors
		public GameViewFeature( Contexts contexts )
		{
			Add( new CreateGameViewSystem( contexts ) );
		}
		#endregion
	}
}
