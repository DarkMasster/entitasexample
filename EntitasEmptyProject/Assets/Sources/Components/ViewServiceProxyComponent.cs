using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace DM.EntitasGame
{
	[ Proxy, Unique ]
	public sealed class ViewServiceProxyComponent : IComponent
	{
		#region Public Fields
		public IViewService instance;
		#endregion
	}
}
