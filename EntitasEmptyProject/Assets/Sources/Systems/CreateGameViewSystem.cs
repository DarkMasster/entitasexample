using System;
using System.Collections.Generic;
using Entitas;

namespace DM.EntitasGame
{
	public sealed class CreateGameViewSystem : ReactiveSystem< GameEntity >, IInitializeSystem
	{
		#region Private Fields
		private Contexts _contexts;
		private IViewService _viewService;
		#endregion

		#region Constructors
		public CreateGameViewSystem( Contexts contexts ) : base( contexts.game )
		{
			_contexts = contexts;
		}
		#endregion

		#region Public Members
		public void Initialize()
		{
			_viewService = _contexts.proxy.viewServiceProxy.instance;
		}
		#endregion

		#region Protected Members
		protected override ICollector< GameEntity > GetTrigger( IContext< GameEntity > context )
		{
			return context.CreateCollector( GameMatcher.Asset );
		}

		protected override bool Filter( GameEntity entity )
		{
			return entity.hasAsset && !entity.hasView;
		}


		protected override void Execute( List< GameEntity > entities )
		{
			foreach( var e in entities )
			{
				try
				{
					var view = _viewService.Instantinate( _contexts, _contexts.game, e, e.asset.prefabPath );
					e.ReplaceView( view );
				}
				catch( Exception exception )
				{
					throw exception;
				}
			}
		}
		#endregion
	}
}
