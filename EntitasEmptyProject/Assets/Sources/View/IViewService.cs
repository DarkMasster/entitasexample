using Entitas;

namespace DM.EntitasGame
{
	public interface IViewService
	{
		#region Public Members
		IViewController Instantinate( Contexts contexts, IContext parentContext, IEntity entity, string prefabPath );
		#endregion
	}
}