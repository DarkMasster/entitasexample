namespace DM.EntitasGame
{
	public class GameFeature : Feature
	{
		#region Constructors
		public GameFeature( Contexts contexts )
		{
			Add( new InitilizeGameSystem( contexts ) );
		}
		#endregion
	}
}