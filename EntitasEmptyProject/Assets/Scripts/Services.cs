﻿namespace DM.EntitasGame
{
	public class Services
	{
		#region Properties
		public IViewService View { get; private set; }
		#endregion

		#region Constructors
		public Services( IViewService view )
		{
			View = view;
		}
		#endregion
	}
}