using Entitas;
using UnityEngine;

namespace DM.EntitasGame
{
	public sealed class InitilizeGameSystem : IInitializeSystem
	{
		#region Private Fields
		private GameContext _context;
		#endregion

		#region Constructors
		public InitilizeGameSystem( Contexts contexts )
		{
			_context = contexts.game;
		}
		#endregion

		#region Public Members
		public void Initialize()
		{
			var entity = _context.CreateEntity();
			entity.AddAsset( "Sphere" );
			entity.AddPosition( new Vector3( 0, 1, 0 ) );
		}
		#endregion
	}
}
